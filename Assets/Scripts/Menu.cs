﻿using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public Button play;
    public Button quit;
    public Button stats;
    public Button settings;
    public Button help;

    public Text musicOn;
    public Text sfxOn;
    public Text musicOff;
    public Text sfxOff;

    public Canvas startMenu;
    public Canvas quitMenu;

    public int highScore;

    public Text scoreText;
    public Canvas statsMenu;
    public Canvas settingsMenu;
    public Canvas helpMenu;

    public bool isMusicOn;
    public bool isSfxOn;

    public Text musicText;
    public Text sfxText;

    public GameOverScript gameOverScript;
    MusicSettings musicSettings;
   

    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1f;
        startMenu = startMenu.GetComponent<Canvas>();
        quitMenu = quitMenu.GetComponent<Canvas>();
        settingsMenu = settingsMenu.GetComponent<Canvas>();
        helpMenu = helpMenu.GetComponent<Canvas>();
        statsMenu = statsMenu.GetComponent<Canvas>();
        play = play.GetComponent<Button>();
        quit = quit.GetComponent<Button>();
        stats = stats.GetComponent<Button>();
        startMenu.enabled = true;
        quitMenu.enabled = false;
        statsMenu.enabled = false;
        settingsMenu.enabled = false;
        helpMenu.enabled = false;
        

       
      //  musicSettings.isSfxOn = true;
       // musicSettings.isMusicOn = true;


        musicSettings = FindObjectOfType<MusicSettings>();
        highScore = PlayerPrefs.GetInt("High Score", gameOverScript.highScore);
    }

    public void ExitPress()
    {
        startMenu.enabled = false;
        quitMenu.enabled = true;
    }

    public void NoPress()
    {
        startMenu.enabled = true;
        quitMenu.enabled = false;


    }

    public void StartLevel()
    {
        Application.LoadLevel(1);
        
    }

    public void ExitGame()
    {

        Application.Quit();
    }


    public void StatsPress()
    {
        startMenu.enabled = false;
        quitMenu.enabled = false;
        statsMenu.enabled = true;
        scoreText.text = "HighScore: " + highScore;
    }

    public void BackPress()
    {
        startMenu.enabled = true;
        quitMenu.enabled = false;
        statsMenu.enabled = false;
    }

    public void SettingsPress()
    {
        startMenu.enabled = false;
        quitMenu.enabled = false;
        statsMenu.enabled = false;
        settingsMenu.enabled = true;
    }
    public void SettingsBackPress()
    {
        startMenu.enabled = true;
        quitMenu.enabled = false;
        statsMenu.enabled = false;
        settingsMenu.enabled = false;
    }

    public void MusicOnPress()
    {

        musicSettings.isMusicOn = true;
        musicOn.color = Color.red;
        musicOff.color = Color.white;

       
           
    }
    public void SfxOnPress()
    {
        musicSettings.isSfxOn = true;
        sfxOn.color = Color.red;
        sfxOff.color = Color.white;
        
           

    }
    public void MusicOffPress()
    {
        musicSettings.isMusicOn = false;
        musicOff.color = Color.red;
        musicOn.color = Color.white;
        

    }
    
    public void SfxOffPress()
    {
        musicSettings.isSfxOn = false;
        sfxOff.color = Color.red;
        sfxOn.color = Color.white;
        
    }

    public void OnHelpPress()
    {
        startMenu.enabled = false;
        quitMenu.enabled = false;
        helpMenu.enabled = true;
    }
    
    public void OnHelpBackPress()
    {
        startMenu.enabled = true;
        quitMenu.enabled = false;
        statsMenu.enabled = false;
        settingsMenu.enabled = false;
        helpMenu.enabled = false;
    }
}
