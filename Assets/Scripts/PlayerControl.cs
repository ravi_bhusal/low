﻿using UnityEngine;
using UnityEngine.Advertisements;

public class PlayerControl : MonoBehaviour
{
    public float maxSpeed = 5f;
    public bool dead = false;

    public float jumpForce = 400f;
    Rigidbody2D rb2D;
    public Transform groundCheck;
    public float groundCheckRadius = 0.2f;
    public bool ground = false;
    public LayerMask whatIsGround;

    public Transform end;

    public Score mScore;
    public GameOverScript gScore;
    
    public int score = 0;

	public  float move=0f;
    public Transform gameOverPrefab;


    public AudioSource jumpAudio;
    public AudioSource deathAudio;
    public AudioSource coinPick;
    public AudioSource music;
 

    MusicSettings musicSettings;

    
    public int i;
    void Awake()
    {
        Time.timeScale = 1f;
        rb2D = GetComponent<Rigidbody2D>();
       
        musicSettings = FindObjectOfType<MusicSettings>();
        music.Play();
        PlayerPrefs.SetInt("Number of Deaths", PlayerPrefs.GetInt("Number of Deaths"));

    }

   


    void FixedUpdate()
    {
      
        ground = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
        

        if (Input.mousePosition.x>Screen.width/2 && Input.GetMouseButton(0))
        {
			if(score<500)
			{
				move=1f;
			}
			else if(score>500 && score<1500)
			{
				move=1.25f;
			}
			else if(score>1500)
			{
				move=1.5f;
			}

            
           
        }
        else if(Input.mousePosition.x < Screen.width/2 && Input.GetMouseButton(0))
        {

			if(score<500)
			{
				move=-1f;
			}
			else if(score>500 && score<1500)
			{
				move=-1.25f;
			}
			else if(score>1500)
			{
				move=-1.5f;
			}
        }
       else if(!Input.GetMouseButtonDown(0))
		{
			move=0f;
		}


		rb2D.velocity = new Vector2(move * maxSpeed, rb2D.velocity.y);



        if (dead == false)
        {
           
            mScore.SetScore(score);
            
            

        }
       
       
       
        gScore.GetYourScore(score);


        if (musicSettings.isMusicOn == false)
        {
            music.mute = true;
        }



    }
    
   

    void Update()
    {
       
       
        if (transform.position.y < -5f)
        {
            Destroy(gameObject);
            dead = true;
            

        }
        if (dead == true)
        {
            
            Time.timeScale = 0;
            Instantiate(gameOverPrefab, Camera.main.transform.position, Quaternion.identity);
            
            
        }
        
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Killer")
        {
            dead = true;
            Destroy(gameObject);
            

        }
        if (dead == true)
        {
            Time.timeScale = 0;
            Instantiate(gameOverPrefab, Camera.main.transform.position, Quaternion.identity);
           
        }
        if(dead==true && musicSettings.isSfxOn==true)
        {
            deathAudio.Play();
        }
        if (col.gameObject.tag == "Platform" && musicSettings.isSfxOn == true)
        {
            jumpAudio.Play();
        }
        if (col.gameObject.tag == "Coins" && musicSettings.isSfxOn == true)
        {
            coinPick.Play();
        }
        

    }
    
        
    
    


}
