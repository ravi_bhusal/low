﻿using UnityEngine;


public class CloudSpawners : MonoBehaviour
{

    public Transform[] cloudPrefab;
    public Transform[] spawnPoint;

	void Start ()
    {
        InvokeRepeating("SpawnCloud", 1f, 5f);
	}
	void SpawnCloud()
    {
        Instantiate(cloudPrefab[Random.Range(0, 2)], spawnPoint[Random.Range(0, 3)].transform.position, Quaternion.identity);
    }
	
}
