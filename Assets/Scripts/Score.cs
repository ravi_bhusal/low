﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class Score : MonoBehaviour {

    public Text text;
    public Text pause;
    public Text play;
    public bool isPaused = false;
    PlayerControl playerControl;
    public int i;
    
    void Start()
    {
        pause.enabled = true;
        play.enabled = false;
        playerControl = FindObjectOfType<PlayerControl>();
      
        
       
    }
    public  void SetScore(float _score)
    {
        text.text = ""+_score;
    }

    public void OnPaused()
    {

        Time.timeScale = 0.0f;
        pause.enabled = false;
        play.enabled = true;
        i = Random.Range(0, 2);
        if (i == 1)
        {
            Advertisement.Show();
            i = 0;
        }
    }
    public void OnPlay()
    {
        Time.timeScale = 1f;
        pause.enabled = true;
        play.enabled = false;
    }

   void Update()
    {
        if(playerControl.dead==true)
        {
            pause.enabled = false;
            play.enabled = false;
        }
    }
}

