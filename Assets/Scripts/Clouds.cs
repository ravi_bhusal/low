﻿using UnityEngine;


public class Clouds : MonoBehaviour {

    Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        rb.velocity = new Vector2(1f, 0f);
        if(transform.position.x>4f)
        {
            Destroy(gameObject);
        }
    }

}
