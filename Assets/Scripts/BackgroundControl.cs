﻿using UnityEngine;


public class BackgroundControl : MonoBehaviour {

    Rigidbody2D rb;
	void Start () 
    {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        rb.velocity = new Vector2(0f, 1f);
	}
}
