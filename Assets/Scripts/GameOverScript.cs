﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System.Collections;


public class GameOverScript : MonoBehaviour
{

    public Text yourScore;
    public Text highScoreText;

    public Button yes;
    public Button no;

    public int highScore;



    public int i;
   
   



    void Start()
    {
      

        yes = yes.GetComponent<Button>();
        no = no.GetComponent<Button>();
        yourScore = yourScore.GetComponent<Text>();
        highScoreText = yourScore.GetComponent<Text>();

        PlayerPrefs.SetInt("High Score", PlayerPrefs.GetInt("High Score"));
      
        
        
       

       

    }

    public void RestartPress()
    {
        Application.LoadLevel(1);    
    }

    public void ExitPress()
    {
        Application.LoadLevel(0);
      
        
    }

    public void GetYourScore(int _score)
    {
        
       
        yourScore.text = "Your Score:" + _score;
        if (_score > PlayerPrefs.GetInt("High Score"))
        {

            PlayerPrefs.SetInt("High Score", _score);
            PlayerPrefs.Save();

                  


        }
        highScoreText.text = "HIGH SCORE:"+PlayerPrefs.GetInt("High Score");
    }

   
   
}