﻿using UnityEngine;
using System.Collections;

public class MusicSettings : MonoBehaviour {

    public bool isMusicOn;
    public bool isSfxOn;
    GameObject gm;

    void Start()
    {
        isMusicOn = true;
        isSfxOn = true;
    }
    void Awake()
    {
        
        DontDestroyOnLoad(transform.gameObject);

        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }

    }


}
