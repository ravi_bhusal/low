﻿using UnityEngine;


public class PlatformControl : MonoBehaviour
{
  

    Rigidbody2D rb2D;
   PlayerControl playerControl;
   public float score;
    public bool collided =false;
	void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
        playerControl = FindObjectOfType<PlayerControl>();
	}



    void FixedUpdate()
    {


        if (playerControl.score < 500)
        {
            rb2D.velocity = new Vector2(0f, 3f);
        }
        else if(playerControl.score>500 && playerControl.score<1000)
        {
            rb2D.velocity = new Vector2(0f, 3.5f);
        }
        else if(playerControl.score>100)
        {
            rb2D.velocity = new Vector2(0f, 4f);
        }
       
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag=="Killer")
        {
            Destroy(gameObject, 0.01f);
        }
      
    }
    
}
