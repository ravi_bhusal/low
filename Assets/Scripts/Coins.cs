﻿using UnityEngine;
using System.Collections;

public class Coins : MonoBehaviour
{
    PlayerControl playerControl;
    

    void Start()
    {
        playerControl = FindObjectOfType<PlayerControl>();
       
    }
	
    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag=="Player")
        {
            Destroy(gameObject,0.00001f);
            playerControl.score += 10;

        }
        if(col.gameObject.tag=="Killer")
        {
            Destroy(gameObject, 0.00001f);
        }
    }
}
