﻿using UnityEngine;


public class Spawners : MonoBehaviour {


    public Transform[] spawnPoints;
    public GameObject[] platformPrefab;
   
    public PlayerControl playerControl;
	GameObject nextPrefab;
	GameObject lastPrefab;

	void Start()
    {
       
            InvokeRepeating("SpawnPlatforms", 0.7f, 0.8f);
       
        
    }

    void SpawnPlatforms()
    {
		do
		{
			nextPrefab=platformPrefab[Random.Range(0,8)];
		}while(nextPrefab==lastPrefab);

        Instantiate(nextPrefab, spawnPoints[Random.Range(0,3)].transform.position, Quaternion.identity);
		lastPrefab=nextPrefab;
    }
}
